module github.com/xplorfin/netutils

go 1.15

require (
	github.com/Flaque/filet v0.0.0-20201012163910-45f684403088
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/brianvoe/gofakeit/v5 v5.11.2
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fasthttp/router v1.3.5
	github.com/gophercloud/gophercloud v0.15.0
	github.com/jarcoal/httpmock v1.0.7
	github.com/jpillora/backoff v1.0.0
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/r3labs/diff/v2 v2.9.1
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/spf13/afero v1.5.1 // indirect
	github.com/urfave/cli/v2 v2.3.0 // indirect
	github.com/valyala/fasthttp v1.19.0
	github.com/yudai/gojsondiff v1.0.0
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
)
